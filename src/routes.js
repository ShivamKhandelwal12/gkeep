import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Notes from './components/Notes/notes';
import Archive from './components/arch';
import Search from './components/search';

export const AppRouter = () => {
  return (
    <>
      <Switch>
        <Route path="/" exact render={props => <Notes {...props} />} />
        <Route path="/archive" render={props => <Archive {...props} />} />
        <Route path="/search" render={props => <Search {...props} />} />
      </Switch>
    </>
  );
};
