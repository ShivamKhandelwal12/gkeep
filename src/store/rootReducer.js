import { combineReducers } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import { notes, currentNote } from '../components/createNoteInput/reducer';
import { pinned, archive } from '../components/arch/reducer';
import { search } from '../components/search/reducer';

const persistConfig = {
  key: 'root',
  storage,
  stateReconciler: autoMergeLevel2
};
const rootReducer = combineReducers({
  notes,
  pinned,
  archive,
  search,
  currentNote
});
const persistedReducer = persistReducer(persistConfig, rootReducer);
export default persistedReducer;
