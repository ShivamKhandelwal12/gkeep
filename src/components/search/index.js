import React from 'react';
import { connect } from 'react-redux';
import Masonary from '../masonaryGrid/masonary';
import { deleteItem } from '../Notes/notes';
import { searchReorder } from './action';
import {
  storePinnedRequest,
  deletePinnedRequest,
  storeArchiveRequest,
  deleteArchiveRequest
} from '../arch/action';
import { deleteNoteRequest, storeNoteRequest } from '../createNoteInput/action';
import './search.scss';

class Search extends React.PureComponent {
  constructor(props) {
    super(props);
    this.renderItems = this.renderItems.bind(this);
    this.handlePinnedClick = this.handlePinnedClick.bind(this);
    this.handleArchiveClick = this.handleArchiveClick.bind(this);
    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.renderSearchResult = this.renderSearchResult.bind(this);
    this.renderNoSearchFound = this.renderNoSearchFound.bind(this);
  }

  filterData = (data, id) => {
    const result = data.filter(note => note.id !== id);
    return result instanceof Array ? result : [];
  };

  handleSearchDelete(context, id) {
    const {
      search: { key, data },
      fnSearchReorder
    } = this.props;
    const copyData = JSON.parse(JSON.stringify(data));
    let pinnedResult = copyData.pinnedResult;
    let archiveResults = copyData.archiveResults;
    let notesResult = copyData.notesResult;
    if (context === 'Pinned') {
      pinnedResult = this.filterData(pinnedResult, id);
    } else if (context === 'Archive') {
      archiveResults = this.filterData(archiveResults, id);
    } else {
      notesResult = this.filterData(notesResult, id);
    }

    fnSearchReorder({
      key,
      data: { pinnedResult, archiveResults, notesResult }
    });
  }

  handleSearchReorder = (fromContext, toContext, item) => {
    const {
      search: { key, data },
      fnSearchReorder
    } = this.props;
    const dataCopy = JSON.parse(JSON.stringify(data));
    let pinnedResult = dataCopy.pinnedResult;
    let archiveResults = dataCopy.archiveResults;
    let notesResult = dataCopy.notesResult;
    if (fromContext === 'Pinned') {
      // remove from pinnedSearch and add to noteSearch
      pinnedResult = this.filterData(pinnedResult, item.id);
      if (toContext === 'Notes') {
        notesResult = [...notesResult, item];
      } else {
        archiveResults = [...archiveResults, item];
      }
    } else if (fromContext === 'Archive') {
      // remove from archiveSearch and add to pinSearch
      archiveResults = this.filterData(archiveResults, item.id);
      if (toContext === 'Pinned') {
        pinnedResult = [...pinnedResult, item];
      } else {
        notesResult = [...notesResult, item];
      }
    } else if (fromContext === 'Notes' && toContext === 'Pinned') {
      notesResult = this.filterData(notesResult, item.id);
      if (toContext === 'Pinned') {
        pinnedResult = [...pinnedResult, item];
      } else {
        archiveResults = [...archiveResults, item];
      }
    }
    fnSearchReorder({
      key,
      data: { pinnedResult, archiveResults, notesResult }
    });
  };

  handlePinnedClick(item, isPinnedNote, context) {
    console.log('INSIDE PINNED CLICK');
    const {
      notes,
      pinned,
      fnRemoveNoteFromPinned,
      fnAddToNotes,
      fnAddNoteToPinned,
      archive,
      fnRemoveNoteFromArchive,
      fnRemoveFromNotes
    } = this.props;
    if (isPinnedNote) {
      // remove from pinnedResult and pinned and store to notes and notesResult
      deleteItem(pinned, fnRemoveNoteFromPinned, item);
      fnAddToNotes(item);
      this.handleSearchReorder(context, 'Notes', item);
    } else {
      // add note to pinned
      fnAddNoteToPinned(item);
      // remove from notes or archive
      if (context === 'Archive') {
        deleteItem(archive, fnRemoveNoteFromArchive, item);
        this.handleSearchReorder(context, 'Pinned', item);
      } else {
        deleteItem(notes, fnRemoveFromNotes, item);
        this.handleSearchReorder(context, 'Pinned', item);
      }
    }
  }

  handleArchiveClick(item, isPinnedNote, context) {
    const {
      fnRemoveNoteFromPinned,
      pinned,
      fnAddNoteToArchive,
      archive,
      fnRemoveNoteFromArchive,
      fnAddToNotes,
      notes,
      fnRemoveFromNotes
    } = this.props;
    if (isPinnedNote) {
      // remove from pinned and store to Archive
      deleteItem(pinned, fnRemoveNoteFromPinned, item);
      fnAddNoteToArchive(item);
      this.handleSearchReorder(context, 'Archive', item);
    } else {
      if (context === 'Archive') {
        // remove from archive and add to notes
        deleteItem(archive, fnRemoveNoteFromArchive, item);
        fnAddToNotes(item);
        this.handleSearchReorder(context, 'Notes', item);
      } else {
        deleteItem(notes, fnRemoveFromNotes, item);
        fnAddNoteToArchive(item);
        this.handleSearchReorder(context, 'Notes', item);
      }
    }
  }

  handleDeleteClick(item, isPinnedNote, context) {
    const {
      pinned,
      fnRemoveNoteFromPinned,
      archive,
      fnRemoveNoteFromArchive,
      notes,
      fnRemoveFromNotes
    } = this.props;
    if (isPinnedNote) {
      deleteItem(pinned, fnRemoveNoteFromPinned, item);
      this.handleSearchDelete(context, item.id);
    } else if (context === 'Archive') {
      deleteItem(archive, fnRemoveNoteFromArchive, item);
      this.handleSearchDelete(context, item.id);
    } else {
      deleteItem(notes, fnRemoveFromNotes, item);
      this.handleSearchDelete(context, item.id);
    }
  }

  renderItems(data, title, isPinned) {
    return (
      <div>
        <p className="sub-heading">{title}</p>
        <Masonary
          data={data}
          parentSelector="search"
          handleModalOpenClose={() => {}}
          isPinned={isPinned}
          handlePinnedClick={this.handlePinnedClick}
          handleDeleteClick={this.handleDeleteClick}
          handleArchiveClick={this.handleArchiveClick}
          context={title}
        />
      </div>
    );
  }

  renderSearchResult() {
    const {
      key,
      data: { pinnedResult, archiveResults, notesResult }
    } = this.props.search;
    return (
      <>
        <h3 className="text-center">Showing Search Rsult for {key}</h3>
        <div className="search__results">
          {pinnedResult.length
            ? this.renderItems(pinnedResult, 'Pinned', true)
            : null}
          {archiveResults.length
            ? this.renderItems(archiveResults, 'Archive', false)
            : null}
          {notesResult.length
            ? this.renderItems(notesResult, 'Notes', false)
            : null}
        </div>
      </>
    );
  }

  renderNoSearchFound() {
    return <p className="sub-heading search__noresult">No Search Found</p>;
  }

  render() {
    const {
      data: { pinnedResult, archiveResults, notesResult }
    } = this.props.search;
    return (
      <div className="search">
        {pinnedResult.length || archiveResults.length || notesResult.length
          ? this.renderSearchResult()
          : this.renderNoSearchFound()}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  search: state.search,
  notes: state.notes,
  pinned: state.pinned,
  archive: state.archive
});

const mapDispatchToProps = dispatch => ({
  fnAddToNotes: data => dispatch(storeNoteRequest(data)),
  fnRemoveNoteFromPinned: data => dispatch(deletePinnedRequest(data)),
  fnAddNoteToArchive: data => dispatch(storeArchiveRequest(data)),
  fnRemoveNoteFromArchive: data => dispatch(deleteArchiveRequest(data)),
  fnRemoveFromNotes: data => dispatch(deleteNoteRequest(data)),
  fnAddNoteToPinned: data => dispatch(storePinnedRequest(data)),
  fnSearchReorder: data => dispatch(searchReorder(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Search);
