import { SEARCH_REQUEST, SEARCH_REORDER } from './action';

export const search = (state = { key: '', data: {} }, action) => {
  const { type, payload } = action;
  switch (type) {
    case SEARCH_REQUEST:
      return Object.assign({}, state, {
        key: payload.key,
        data: payload.data
      });
    case SEARCH_REORDER:
      return Object.assign({}, state, { key: payload.key, data: payload.data });
    default:
      return state;
  }
};
