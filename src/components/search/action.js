export const SEARCH_REQUEST = 'search-request';
export const SEARCH_REORDER = 'search-reorder';

export const searchRequest = data => ({
  type: SEARCH_REQUEST,
  payload: data
});

export const searchReorder = data => ({
  type: SEARCH_REORDER,
  payload: data
});
