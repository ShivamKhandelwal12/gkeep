import React from 'react';
import './masonary.scss';
import Card from './card';

class Masonary extends React.Component {
  constructor(props) {
    super(props);
    this.renderItems = this.renderItems.bind(this);
    this.resizeAllGridItems = this.resizeAllGridItems.bind(this);
    this.resizeGridItem = this.resizeGridItem.bind(this);
  }

  componentDidMount() {
    const { parentSelector } = this.props;
    this.resizeAllGridItems(parentSelector);
  }

  componentDidUpdate(prevPros) {
    console.log('INSIDE COMPONENT DID UPDATE: ', this.props.noteEdited);
    const { data, parentSelector, noteEdited } = this.props;
    if (JSON.stringify(prevPros.data) !== JSON.stringify(data) || noteEdited) {
      console.log(
        'INSIDE COMPONENT DID UPDATE updating: ',
        this.props.noteEdited
      );
      setTimeout(() => {
        this.resizeAllGridItems(parentSelector);
      }, 0);
    }
  }

  resizeGridItem(item) {
    const grid = document.getElementsByClassName('masonary__grid')[0];
    const rowHeight = parseInt(
      window.getComputedStyle(grid).getPropertyValue('grid-auto-rows')
    );
    const rowGap = parseInt(
      window.getComputedStyle(grid).getPropertyValue('grid-row-gap')
    );
    const rowspan = Math.ceil(
      (item.querySelector('.content').getBoundingClientRect().height + rowGap) /
        (rowHeight + rowGap)
    );
    const maxRowSpan = Math.ceil((400 + rowGap) / (rowHeight + rowGap));
    const applyMaxRowSpan = rowspan > maxRowSpan ? true : false;
    if (applyMaxRowSpan) {
      item.style.gridRowEnd = 'span ' + maxRowSpan;
      item.querySelector('.masonary__item__button').style.display = 'block';
      item.querySelector('.masonary__item__desc').style.height =
        maxRowSpan * rowHeight - 2 * rowGap + 'px';
    } else {
      item.style.gridRowEnd = 'span ' + (rowspan + 1);
    }
  }

  resizeAllGridItems(parentSelector) {
    const allItems = document
      .getElementsByClassName(parentSelector)[0]
      .querySelectorAll('.masonary__item');
    for (let x = 0; x < allItems.length; x++) {
      this.resizeGridItem(allItems[x]);
    }
  }

  renderItems() {
    const {
      data,
      handleModalOpenClose,
      isPinned,
      parentSelector,
      handlePinnedClick,
      handleArchiveClick,
      handleDeleteClick,
      context = ''
    } = this.props;
    return data.map((item, index) => {
      return (
        <Card
          item={item}
          index={index}
          handleModalOpenClose={handleModalOpenClose}
          key={index}
          isPinned={isPinned}
          parentSelector={parentSelector}
          handlePinnedClick={handlePinnedClick}
          handleArchiveClick={handleArchiveClick}
          handleDeleteClick={handleDeleteClick}
          context={context}
        />
      );
    });
  }

  render() {
    console.log('Logging is rendered: ', this.props.noteEdited);
    return <div className="masonary masonary__grid">{this.renderItems()}</div>;
  }
}

export default Masonary;
