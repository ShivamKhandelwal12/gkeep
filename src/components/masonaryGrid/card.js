import React from 'react';
import { connect } from 'react-redux';
import { AiOutlinePushpin, AiFillPushpin } from 'react-icons/ai';
import { MdArchive, MdUnarchive } from 'react-icons/md';
import { FaRegTrashAlt } from 'react-icons/fa';
import {
  storePinnedRequest,
  deletePinnedRequest,
  storeArchiveRequest,
  deleteArchiveRequest
} from '../arch/action';
import { deleteNoteRequest, storeNoteRequest } from '../createNoteInput/action';

const Card = props => {
  const {
    index,
    item,
    handleModalOpenClose,
    isPinned,
    parentSelector,
    handlePinnedClick,
    handleArchiveClick,
    handleDeleteClick,
    context
  } = props;

  const renderArchiveBtn = () => {
    if (parentSelector === 'archive') {
      return (
        <button
          className="masonary__item__archive"
          onClick={() => handleArchiveClick(item, isPinned, context)}
        >
          <MdUnarchive size="20" />
        </button>
      );
    } else {
      return (
        <button
          className="masonary__item__archive"
          onClick={() => handleArchiveClick(item, isPinned, context)}
        >
          <MdArchive size="20" />
        </button>
      );
    }
  };

  return (
    <div
      className={`masonary__item ${isPinned ? 'pinned_card' : ''}`}
      key={index}
      id={index}
    >
      <div className="content">
        <div className="masonary__item__title">
          <h3 onClick={() => handleModalOpenClose(item, context, isPinned)}>
            {item.title}
          </h3>
          <button
            className="pin_icon"
            onClick={() => handlePinnedClick(item, isPinned, context)}
          >
            {isPinned ? (
              <AiFillPushpin size="20" />
            ) : (
              <AiOutlinePushpin size="20" />
            )}
          </button>
        </div>
        <div
          className="masonary__item__desc"
          onClick={() => handleModalOpenClose(item, context, isPinned)}
        >
          {item.desc}
        </div>
        <div className="masonary__item__showmore">
          {renderArchiveBtn()}
          <button
            className="masonary__item__button masonary__item__showmore__button"
            onClick={() => handleModalOpenClose()}
          >
            ShowMore
          </button>
          <button
            className="masonary__item__trash"
            onClick={() => handleDeleteClick(item, isPinned, context)}
          >
            <FaRegTrashAlt size="20" />
          </button>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  notes: state.notes,
  pinned: state.pinned,
  archive: state.archive
});

const mapDispatchToProps = dispatch => ({
  fnSetPinnedNote: data => dispatch(storePinnedRequest(data)),
  fnRemoveItemFromNote: data => dispatch(deleteNoteRequest(data)),
  fnStoreNoteRequest: data => dispatch(storeNoteRequest(data)),
  fnRemoveItemFromPinned: data => dispatch(deletePinnedRequest(data)),
  fnAddItemToArchive: data => dispatch(storeArchiveRequest(data)),
  fnDeleteItemFromArchive: data => dispatch(deleteArchiveRequest(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Card);
