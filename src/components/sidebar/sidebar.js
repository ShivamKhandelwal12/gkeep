import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { GoNote } from 'react-icons/go';
import { MdArchive, MdClose } from 'react-icons/md';
import './sidebar.scss';

class SideBar extends React.PureComponent {
  constructor(props) {
    super(props);
    this.renderListView = this.renderListView.bind(this);
  }

  renderListView(link, text, Icon) {
    const {
      location: { pathname }
    } = this.props;
    console.log('PATH NAME: ', pathname);
    console.log('LINK: ', link);
    return (
      <Link to={link}>
        <div
          className={`sidebar__list ${
            pathname === link ? 'sidebar__list__selected' : ''
          }`}
        >
          <Icon size="22" />
          <h3 className="sidebar__list__heading">{text}</h3>
        </div>
      </Link>
    );
  }
  render() {
    const { showMenu, handleMenuToggle } = this.props;
    console.log('PROPS: ', this.props);
    return (
      <div
        className={`sidebar ${
          showMenu ? 'sidebar__width' : 'sidebar__transform'
        }`}
      >
        <div className="sidebar__header">
          <h2>keep</h2>
          <button
            onClick={() => handleMenuToggle()}
            className="sidebar__header__close__btn"
          >
            <MdClose size="22" />
          </button>
        </div>
        {this.renderListView('/', 'Notes', GoNote)}
        {this.renderListView('/archive', 'Archive', MdArchive)}
      </div>
    );
  }
}

export default withRouter(SideBar);
