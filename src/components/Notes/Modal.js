import React, { useState, useEffect, useLayoutEffect } from 'react';
import RModal from 'react-modal';
import { connect } from 'react-redux';
import { AiOutlinePushpin, AiFillPushpin } from 'react-icons/ai';
import { MdArchive } from 'react-icons/md';
import {
  editNoteDone,
  updateNote,
  deleteNoteRequest,
  storeNoteRequest
} from '../createNoteInput/action';
import {
  storePinnedRequest,
  storeArchiveRequest,
  deletePinnedRequest,
  deleteArchiveRequest,
  updateArchive,
  updatePinned
} from '../arch/action';
import { deleteItem } from './notes';
import './notes.scss';

const Modal = props => {
  const {
    currentNote: {
      isModalOpen,
      data: { item, context, isPinnedNote }
    }
  } = props;
  const [noteTitle, setNoteTitle] = useState(
    item && item.title ? item.title : ''
  );
  const [noteDesc, setNoteDesc] = useState(item && item.desc ? item.desc : '');
  const [isArchive, setArchive] = useState(
    context === 'Archive' ? true : false
  );
  const [isPinned, setPinned] = useState(isPinnedNote);
  const [heightRendered, setHeightRendered] = useState(false);
  const noteDescRef = React.createRef();

  const handleModalClose = () => {
    handleSaveClick();
    setHeightRendered(!heightRendered);
    props.fnEditNoteDone();
  };

  useEffect(() => {
    if (item && Object.keys(item).length) {
      setNoteDesc(item.desc);
      setNoteTitle(item.title);
      setPinned(isPinnedNote);
      setArchive(context === 'Archive' ? true : false);
    }
  }, [item, isPinnedNote]);

  useLayoutEffect(() => {
    setTimeout(() => {
      if (noteDescRef.current !== null && !heightRendered) {
        const height = noteDescRef.current.scrollHeight;
        noteDescRef.current.style.height = height + 'px';
        setHeightRendered(!heightRendered);
      }
    }, 0);
  }, [noteDescRef]);

  useEffect(() => {
    RModal.setAppElement('body');
  }, []);

  const handleOnChange = e => {
    const el = e.target;
    el.style.cssText = 'height:auto; padding: 0';
    el.style.cssText = '-moz-box-sizing:content-box';
    el.style.cssText = 'height: ' + el.scrollHeight + 'px';
    if (e.target.name === 'desc') {
      setNoteDesc(e.target.value);
    } else {
      setNoteTitle(e.target.value);
    }
  };

  const handlePinnedClick = () => {
    setPinned(!isPinned);
    if (isArchive) {
      setArchive(!isArchive);
    }
  };

  const handleArchiveClick = () => {
    setArchive(!isArchive);
    if (isPinned) {
      setPinned(!isPinned);
    }
  };

  const updateData = (dataCopy, dispatcher) => {
    for (let note of dataCopy) {
      if (note.id === item.id) {
        note.title = noteTitle;
        note.desc = noteDesc;
        break;
      }
    }
    dispatcher(dataCopy);
  };

  const handleSaveClick = () => {
    const {
      notes,
      archive,
      pinned,
      fnRemoveFromNote,
      fnAddNoteToPinned,
      fnRemoveNoteFromArchive,
      fnAddNoteToArchive,
      fnRemoveItemFromPinned,
      fnAddToNote,
      fnUpdatePinnedNote,
      fnUpdateArchiveNote,
      fnUpdateNote
    } = props;
    // note has been updated
    if (context === 'Notes') {
      // update to pinned notes
      // if context is Notes remove from notes and add to pinned
      if (isPinned) {
        deleteItem(notes, fnRemoveFromNote, item);
        fnAddNoteToPinned(item);
      } else if (isArchive) {
        // if context is Archive remove from archive and add to pinned
        // deleteItem(archive, fnRemoveNoteFromArchive, item);
        deleteItem(notes, fnRemoveFromNote, item);
        fnAddNoteToArchive(item);
      } else {
        // if the context is Pinned update item to pinned
        if (noteTitle !== item.title || noteDesc !== item.desc) {
          updateData(notes.data, fnUpdateNote);
        }
      }
    } else if (context === 'Archive') {
      // if context is Notes remove from note and add to archive
      if (isPinned) {
        deleteItem(archive, fnRemoveNoteFromArchive, item);
        fnAddNoteToPinned(item);
      } else if (!isArchive) {
        // remove item from pinned and add to archive
        deleteItem(archive, fnRemoveNoteFromArchive, item);
        fnAddToNote(item);
      } else {
        // update archive note
        if (noteTitle !== item.title || noteDesc !== item.desc) {
          updateData(archive.data, fnUpdateArchiveNote);
        }
      }
    } else {
      if (!isPinned) {
        // remove note from pinned and add to note
        deleteItem(pinned, fnRemoveItemFromPinned, item);
        fnAddToNote(item);
      } else if (isArchive) {
        // remove note from archive and add to note
        // deleteItem(archive, fnRemoveNoteFromArchive, item);
        deleteItem(pinned, fnRemoveItemFromPinned, item);
        fnAddNoteToArchive(item);
      } else {
        // update the note
        if (noteTitle !== item.title || noteDesc !== item.desc) {
          updateData(pinned.data, fnUpdatePinnedNote);
        }
      }
    }
  };
  return (
    <RModal
      isOpen={isModalOpen}
      onRequestClose={() => handleModalClose()}
      overlayClassName="modal__overlay"
      className="modal__content"
    >
      <div className="modal__container">
        <div className="modal__container__title">
          <textarea
            value={noteTitle}
            placeholder="Title"
            onChange={e => handleOnChange(e)}
            name="title"
          ></textarea>
          <button onClick={() => handlePinnedClick()}>
            {isPinned ? (
              <AiFillPushpin size="20" />
            ) : (
              <AiOutlinePushpin size="20" />
            )}
          </button>
        </div>
        <textarea
          value={noteDesc}
          placeholder="Take a Note"
          onChange={e => handleOnChange(e)}
          name="desc"
          className="desc_textarea"
          ref={noteDescRef}
        ></textarea>
        <div className="modal__bottom">
          <button
            onClick={() => handleArchiveClick()}
            className={`button__archive ${isArchive ? 'button__selected' : ''}`}
          >
            <MdArchive size="20" />
          </button>
          <button onClick={() => handleModalClose()} className="close__btn">
            close
          </button>
        </div>
      </div>
    </RModal>
  );
};

const mapStateToProps = state => ({
  currentNote: state.currentNote,
  notes: state.notes,
  pinned: state.pinned,
  archive: state.archive
});

const mapDispatchToProps = dispatch => ({
  fnEditNoteDone: () => dispatch(editNoteDone()),
  fnRemoveFromNote: data => dispatch(deleteNoteRequest(data)),
  fnAddToNote: data => dispatch(storeNoteRequest(data)),
  fnAddNoteToPinned: data => dispatch(storePinnedRequest(data)),
  fnRemoveItemFromPinned: data => dispatch(deletePinnedRequest(data)),
  fnRemoveNoteFromArchive: data => dispatch(deleteArchiveRequest(data)),
  fnAddNoteToArchive: data => dispatch(storeArchiveRequest(data)),
  fnUpdateNote: data => dispatch(updateNote(data)),
  fnUpdatePinnedNote: data => dispatch(updatePinned(data)),
  fnUpdateArchiveNote: data => dispatch(updateArchive(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Modal);
