import React from 'react';
import { connect } from 'react-redux';
import './notes.scss';
// import { data } from './data';
import Masonary from '../masonaryGrid/masonary';
import CreateNote from '../createNoteInput';
import {
  storeArchiveRequest,
  storePinnedRequest,
  deletePinnedRequest
} from '../arch/action';
import {
  deleteNoteRequest,
  storeNoteRequest,
  editNoteRequest
} from '../createNoteInput/action';
import Modal from './Modal';

export const deleteItem = (dataRef, dispatcher, item) => {
  const { data } = dataRef;
  const dataCopy = [...data];
  let itemIndex = -1;
  dataCopy.filter((dataItem, i) => {
    if (dataItem.id === item.id) {
      itemIndex = i;
    }
  });
  if (itemIndex !== -1) {
    dataCopy.splice(itemIndex, 1);
    dispatcher(dataCopy);
  }
};

class Notes extends React.PureComponent {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = {
      showExpanded: false,
      isModalOpen: false,
      title: '',
      desc: ''
    };
    this.renderInputOrNoteTag = this.renderInputOrNoteTag.bind(this);
    this.handleModalOpenClose = this.handleModalOpenClose.bind(this);
    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.renderNotes = this.renderNotes.bind(this);
    this.renderPinnedItems = this.renderPinnedItems.bind(this);
    this.handlePinnedClick = this.handlePinnedClick.bind(this);
    this.handleArchiveClick = this.handleArchiveClick.bind(this);
    this.handleDeleteClick = this.handleDeleteClick.bind(this);
  }

  handlePinnedClick(item, isPinnedNote) {
    // store item to pinned and remove from notes
    const {
      fnAddItemToPinned,
      notes,
      fnRemoveItemFromNotes,
      fnAddItemsToNotes,
      pinned,
      fnRemoveItemsFromPinned
    } = this.props;
    if (isPinnedNote) {
      // remove item from piined and add to note
      fnAddItemsToNotes(item);
      deleteItem(pinned, fnRemoveItemsFromPinned, item);
    } else {
      fnAddItemToPinned(item);
      deleteItem(notes, fnRemoveItemFromNotes, item);
    }
  }

  handleArchiveClick(item, isPinnedNote) {
    const {
      fnAddItemToArchive,
      fnRemoveItemFromNotes,
      fnRemoveItemsFromPinned,
      notes,
      pinned
    } = this.props;
    // add item to archive and remove from notes
    // if item is pinned remove it from both pinned and notes
    fnAddItemToArchive(item);
    if (isPinnedNote) {
      deleteItem(pinned, fnRemoveItemsFromPinned, item);
    } else {
      deleteItem(notes, fnRemoveItemFromNotes, item);
    }
  }

  handleDeleteClick(item, isPinnedNote) {
    const {
      notes,
      pinned,
      fnRemoveItemFromNotes,
      fnRemoveItemsFromPinned
    } = this.props;
    if (isPinnedNote) {
      deleteItem(pinned, fnRemoveItemsFromPinned, item);
    } else {
      deleteItem(notes, fnRemoveItemFromNotes, item);
    }
  }

  handleTitleChange() {
    console.log('TARGET NAME: ', this.myRef.current.innerHTML);
  }

  renderPinnedItems() {
    const { data } = this.props.pinned;
    const {
      currentNote: { noteEdited }
    } = this.props;
    if (data.length) {
      return (
        <>
          <p className="sub-heading">Pinned</p>
          <Masonary
            parentSelector="note"
            data={data}
            handleModalOpenClose={this.handleModalOpenClose}
            isPinned={true}
            handlePinnedClick={this.handlePinnedClick}
            handleArchiveClick={this.handleArchiveClick}
            handleDeleteClick={this.handleDeleteClick}
            context="Pinned"
            noteEdited={noteEdited ? noteEdited : false}
          />
        </>
      );
    } else {
      return null;
    }
  }

  renderNotes() {
    const { data } = this.props.notes;
    const {
      currentNote: { noteEdited }
    } = this.props;
    if (data.length) {
      return (
        <Masonary
          data={data}
          parentSelector="note"
          handleModalOpenClose={this.handleModalOpenClose}
          isPinned={false}
          handlePinnedClick={this.handlePinnedClick}
          handleArchiveClick={this.handleArchiveClick}
          handleDeleteClick={this.handleDeleteClick}
          context="Notes"
          noteEdited={noteEdited ? noteEdited : false}
        />
      );
    } else {
      return <h3>No Notes</h3>;
    }
  }

  renderInputOrNoteTag() {
    return <CreateNote />;
  }

  handleModalOpenClose(item, context, isPinnedNote) {
    const { fnEditNote } = this.props;
    fnEditNote({ item, context, isPinnedNote });
  }

  render() {
    const { pinned } = this.props;
    return (
      <div className="note">
        {this.renderInputOrNoteTag()}
        <Modal overlayClassName="modal__overlay" className="modal__content" />
        {this.renderPinnedItems()}
        {pinned.data.length ? <p className="sub-heading">Others</p> : null}
        {this.renderNotes()}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  notes: state.notes,
  pinned: state.pinned,
  currentNote: state.currentNote
});

const mapDispatchToProps = dispatch => ({
  fnAddItemToPinned: data => dispatch(storePinnedRequest(data)),
  fnAddItemToArchive: data => dispatch(storeArchiveRequest(data)),
  fnAddItemsToNotes: data => dispatch(storeNoteRequest(data)),
  fnRemoveItemFromNotes: data => dispatch(deleteNoteRequest(data)),
  fnRemoveItemsFromPinned: data => dispatch(deletePinnedRequest(data)),
  fnEditNote: data => dispatch(editNoteRequest(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Notes);
