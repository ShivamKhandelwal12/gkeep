export const data = [
  {
    title: 'title 1',
    desc: 'Lorem ipsum dolor sit amet'
  },
  {
    title: 'title 2',
    desc:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nisi enim, convallis sed dictum in, rutrum at est. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec varius diam et risus ullamcorper sollicitudin. Mauris pulvinar, risus non accumsan pretium, mauris dolor faucibus risus, et viverra nunc massa eu metus. Maecenas hendrerit magna nulla, a congue ligula faucibus vitae. Suspendisse vitae velit quam. Pellentesque vitae nisi in magna fringilla maximus. Quisque eget massa ac nibh lacinia accumsan non vel lectus. Donec rutrum quis dui a cursus. Phasellus vulputate sollicitudin finibus. Duis laoreet et augue sit amet semper. Donec aliquet tincidunt dui, et laoreet tellus aliquam a. Nulla a urna interdum, tincidunt justo eget, consequat purus. Donec a libero vehicula lorem pellentesque convallis. Aliquam nec euismod nunc, nec facilisis velit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas'
  },
  {
    title: 'title 3',
    desc: 'Lorem ipsum dolor sit amet'
  },
  {
    title: 'title 4',
    desc: 'Lorem ipsum dolor sit amet'
  },
  {
    title: 'title 5',
    desc: 'Lorem ipsum dolor sit amet'
  },
  {
    title: 'title 6',
    desc: 'Lorem ipsum dolor sit amet'
  },
  {
    title: 'title 7',
    desc: 'Lorem ipsum dolor sit amet'
  },
  {
    title: 'title 8',
    desc: 'Lorem ipsum dolor sit amet'
  },
  {
    title: 'title 9',
    desc: 'Lorem ipsum dolor sit amet'
  },
  {
    title: 'title 10',
    desc: 'Lorem ipsum dolor sit amet'
  },
  {
    title: 'title 11',
    desc: 'Lorem ipsum dolor sit amet'
  }
];
