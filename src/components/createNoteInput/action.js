export const STORE_NOTE_REQUEST = 'store-note-request';
export const STORE_NOTE_SUCCESS = 'store-note-success';
export const DELETE_NOTE_REQUEST = 'delete-note-request';
export const UPDATE_NOTE = 'update-note';

export const EDIT_NOTE_REQUEST = 'edit-note-request';
export const EDIT_NOTE_DONE = 'edit-note-done';

export const storeNoteRequest = data => ({
  type: STORE_NOTE_REQUEST,
  payload: data
});

export const storeNoteSuccess = () => ({
  type: STORE_NOTE_SUCCESS
});

export const deleteNoteRequest = data => ({
  type: DELETE_NOTE_REQUEST,
  payload: data
});

export const editNoteRequest = data => ({
  type: EDIT_NOTE_REQUEST,
  payload: data
});

export const editNoteDone = () => ({
  type: EDIT_NOTE_DONE
});

export const updateNote = data => ({
  type: UPDATE_NOTE,
  payload: data
});
