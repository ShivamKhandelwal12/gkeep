import {
  STORE_NOTE_REQUEST,
  STORE_NOTE_SUCCESS,
  DELETE_NOTE_REQUEST,
  EDIT_NOTE_REQUEST,
  EDIT_NOTE_DONE,
  UPDATE_NOTE
} from './action';
export const notes = (state = { isStoreRequest: false, data: [] }, action) => {
  switch (action.type) {
    case STORE_NOTE_REQUEST:
      return Object.assign({}, state, {
        isStoreRequest: true,
        data: [...state.data, action.payload]
      });
    case STORE_NOTE_SUCCESS:
      return Object.assign({}, state, { isStoreRequest: false });
    case DELETE_NOTE_REQUEST:
      return Object.assign({}, state, { data: action.payload });
    case UPDATE_NOTE:
      return Object.assign({}, state, { data: action.payload });
    default:
      return state;
  }
};

export const currentNote = (
  state = { isModalOpen: false, data: {}, noteEdited: false },
  action
) => {
  switch (action.type) {
    case EDIT_NOTE_REQUEST:
      return Object.assign({}, state, {
        isModalOpen: true,
        data: action.payload,
        noteEdited: false
      });
    case EDIT_NOTE_DONE:
      return Object.assign({}, state, { isModalOpen: false, noteEdited: true });
    default:
      return state;
  }
};
