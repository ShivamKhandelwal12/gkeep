import React from 'react';

export const Input = React.forwardRef(
  ({ onInput, onBlur, placeholder, value, className, onFocus }, ref) => {
    return (
      <div
        contentEditable={true}
        placeholder={placeholder}
        onInput={onInput}
        onBlur={onBlur}
        className={className}
        onFocus={onFocus}
        ref={ref}
        dangerouslySetInnerHTML={{ __html: value }}
      ></div>
    );
  }
);
