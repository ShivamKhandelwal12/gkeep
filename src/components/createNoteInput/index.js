import React, { useState } from 'react';
import { connect } from 'react-redux';
import { AiOutlinePushpin, AiFillPushpin } from 'react-icons/ai';
import { MdArchive } from 'react-icons/md';
import { storePinnedRequest, storeArchiveRequest } from '../arch/action';
import { storeNoteRequest } from './action';
import './createNote.scss';

const CreateNote = props => {
  const [expanded, setExpanded] = useState(false);
  const [title, setTitle] = useState('');
  const [desc, setDesc] = useState('');
  const [pinned, setPinned] = useState(false);
  const [isArchive, setArchive] = useState(false);

  const handleSaveClick = () => {
    const {
      fnStoreNote,
      notes,
      fnAddItemToPinned,
      pinnedNotes,
      fnAddItemToArchive,
      archiveNotes
    } = props;
    if (title.length || desc.length) {
      if (pinned) {
        fnAddItemToPinned({ title, desc, id: pinnedNotes.data.length + 1 });
      } else if (isArchive) {
        fnAddItemToArchive({ title, desc, id: archiveNotes.data.length + 1 });
      } else {
        fnStoreNote({ title, desc, id: notes.data.length + 1 });
      }
    }
    setExpanded(false);
    setTitle('');
    setDesc('');
  };

  const handleOnChange = e => {
    const el = e.target;
    el.style.cssText = 'height:auto; padding: 0';
    el.style.cssText = '-moz-box-sizing:content-box';
    el.style.cssText = 'height: ' + el.scrollHeight + 'px';
    if (e.target.name === 'desc') {
      setDesc(e.target.value);
    } else {
      setTitle(e.target.value);
    }
  };

  const handleOnFocus = e => {
    setExpanded(true);
  };

  const handleOnBlur = () => {};

  const handleArchiveClick = () => {
    if (pinned) {
      setPinned(!pinned);
      setArchive(!isArchive);
    } else {
      setArchive(!isArchive);
    }
  };

  const handlePinnedClick = () => {
    if (isArchive) {
      setArchive(!isArchive);
      setPinned(!pinned);
    } else {
      setPinned(!pinned);
    }
  };

  return (
    <div className="createNote">
      <div className="createNote__title__container">
        {expanded ? (
          <button onClick={() => handlePinnedClick()}>
            {pinned ? (
              <AiFillPushpin size="20" />
            ) : (
              <AiOutlinePushpin size="20" />
            )}
          </button>
        ) : null}
        <textarea
          className="createNote__title createNote__area mr-8"
          placeholder="title"
          name="title"
          value={title}
          onChange={e => handleOnChange(e)}
          onFocus={e => handleOnFocus(e)}
          onBlur={() => handleOnBlur()}
        ></textarea>
      </div>
      {expanded ? (
        <>
          <textarea
            className="createNote__desc createNote__area"
            placeholder="Take a Note"
            name="desc"
            value={desc}
            onChange={e => handleOnChange(e)}
          ></textarea>
          <div className="createNote__button__container">
            <button
              onClick={() => handleArchiveClick()}
              className={`button__archive ${
                isArchive ? 'button__selected' : ''
              }`}
            >
              <MdArchive size="20" />
            </button>
            <button
              onClick={() => handleSaveClick()}
              className="button__create"
            >
              Create
            </button>
          </div>
        </>
      ) : null}
    </div>
  );
};

const mapStateToProps = state => ({
  notes: state.notes,
  pinnedNotes: state.pinned,
  archiveNotes: state.archive
});

const mapDispatchToProps = dispatch => ({
  fnStoreNote: data => dispatch(storeNoteRequest(data)),
  fnAddItemToPinned: data => dispatch(storePinnedRequest(data)),
  fnAddItemToArchive: data => dispatch(storeArchiveRequest(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateNote);
