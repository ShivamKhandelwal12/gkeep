import React from 'react';
import { connect } from 'react-redux';
import Masonary from '../masonaryGrid/masonary';
import {
  storeArchiveRequest,
  deleteArchiveRequest,
  storePinnedRequest
} from './action';
import { storeNoteRequest, editNoteRequest } from '../createNoteInput/action';
import { deleteItem } from '../Notes/notes';
import Modal from '../Notes/Modal';
import './archive.scss';

class Archive extends React.PureComponent {
  constructor(props) {
    super(props);
    this.renderitems = this.renderitems.bind(this);
    this.handlePinnedClick = this.handlePinnedClick.bind(this);
    this.handleArchiveClick = this.handleArchiveClick.bind(this);
    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.handleModalOpenClose = this.handleModalOpenClose.bind(this);
  }

  handlePinnedClick(item) {
    // remove note from archive and add to pin
    const { fnRemoveNoteFromArchive, archive, fnAddNoteToPinned } = this.props;
    deleteItem(archive, fnRemoveNoteFromArchive, item);
    fnAddNoteToPinned(item);
  }

  handleArchiveClick(item) {
    // remove note from archive and store to notes
    const { fnRemoveNoteFromArchive, archive, fnAddToNotes } = this.props;
    deleteItem(archive, fnRemoveNoteFromArchive, item);
    fnAddToNotes(item);
  }
  handleDeleteClick(item) {
    // remove note from archive
    const { fnRemoveNoteFromArchive, archive } = this.props;
    deleteItem(archive, fnRemoveNoteFromArchive, item);
  }

  handleModalOpenClose(item, context, isPinnedNote) {
    const { fnEditNote } = this.props;
    fnEditNote({ item, context, isPinnedNote });
  }

  renderitems() {
    const { data } = this.props.archive;
    if (data.length) {
      return (
        <Masonary
          data={data}
          parentSelector="archive"
          handleModalOpenClose={this.handleModalOpenClose}
          isPinned={false}
          handlePinnedClick={this.handlePinnedClick}
          handleArchiveClick={this.handleArchiveClick}
          handleDeleteClick={this.handleDeleteClick}
          context="Archive"
        />
      );
    } else {
      return <h3>No Archives</h3>;
    }
  }
  render() {
    return (
      <div>
        <div className="archive">{this.renderitems()}</div>
        <Modal
          // isModalOpen={isModalOpen}
          // handleModalOpenClose={this.handleModalOpenClose}
          overlayClassName="modal__overlay"
          className="modal__content"
          title="hello"
          desc="hello desc"
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  archive: state.archive,
  pinned: state.pinned,
  notes: state.notes
});

const mapDispatchToProps = dispatch => ({
  fnStoreItemToArchive: data => dispatch(storeArchiveRequest(data)),
  fnRemoveNoteFromArchive: data => dispatch(deleteArchiveRequest(data)),
  fnAddNoteToPinned: data => dispatch(storePinnedRequest(data)),
  fnAddToNotes: data => dispatch(storeNoteRequest(data)),
  fnEditNote: data => dispatch(editNoteRequest(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Archive);
