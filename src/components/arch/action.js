export const STORED_PINNED_REQUEST = 'store-pinned-request';
export const STORE_PINNED_SUCCESS = 'store-pinned-success';
export const DELETE_PINNED_REQUEST = 'delete-pinned-request';

export const STORE_ARCHIVE_REQUEST = 'store-archive-request';
export const DELETE_ARCHIVE_REQUEST = 'delete-archive-request';

export const UPDATE_PINNED = 'update-pinned';
export const UPDATE_ARCHIVE = 'update-archive';

export const storePinnedRequest = data => ({
  type: STORED_PINNED_REQUEST,
  payload: data
});

export const storePinnedSuccess = () => ({
  type: STORE_PINNED_SUCCESS
});

export const deletePinnedRequest = data => ({
  type: DELETE_PINNED_REQUEST,
  payload: data
});

export const storeArchiveRequest = data => ({
  type: STORE_ARCHIVE_REQUEST,
  payload: data
});

export const deleteArchiveRequest = data => ({
  type: DELETE_ARCHIVE_REQUEST,
  payload: data
});

export const updatePinned = data => ({
  type: UPDATE_PINNED,
  payload: data
});

export const updateArchive = data => ({
  type: UPDATE_ARCHIVE,
  payload: data
});
