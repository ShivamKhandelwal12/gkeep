import {
  STORED_PINNED_REQUEST,
  STORE_PINNED_SUCCESS,
  DELETE_PINNED_REQUEST,
  STORE_ARCHIVE_REQUEST,
  DELETE_ARCHIVE_REQUEST,
  UPDATE_ARCHIVE,
  UPDATE_PINNED
} from './action';
export const pinned = (state = { isStoreRequest: false, data: [] }, action) => {
  switch (action.type) {
    case STORED_PINNED_REQUEST:
      return Object.assign({}, state, {
        isStoreRequest: false,
        data: [...state.data, action.payload]
      });
    case STORE_PINNED_SUCCESS:
      return Object.assign({}, state, {
        isStoreRequest: false,
        data: [...state.data, action.payload]
      });
    case DELETE_PINNED_REQUEST:
      return Object.assign({}, state, { data: action.payload });
    case UPDATE_PINNED:
      return Object.assign({}, state, { data: action.payload });
    default:
      return state;
  }
};

export const archive = (
  state = { isStoreRequest: false, data: [] },
  action
) => {
  switch (action.type) {
    case STORE_ARCHIVE_REQUEST:
      return Object.assign({}, state, {
        isStoreRequest: true,
        data: [...state.data, action.payload]
      });
    case DELETE_ARCHIVE_REQUEST:
      return Object.assign({}, state, {
        isStoreRequest: false,
        data: action.payload
      });
    case UPDATE_ARCHIVE:
      return Object.assign({}, state, {
        isStoreRequest: false,
        data: action.payload
      });
    default:
      return state;
  }
};
