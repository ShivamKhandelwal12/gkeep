import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { MdMenu } from 'react-icons/md';
import { searchRequest } from '../search/action';
import './header.scss';

class Header extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      isDark: false
    };
    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.setSearchResults = this.setSearchResults.bind(this);
    this.filterData = this.filterData.bind(this);
    this.handleSearchBlur = this.handleSearchBlur.bind(this);
  }

  // componentDidUpdate(prevProps) {
  //   if(JSON.stringify)
  // }

  filterData(data, searchVal) {
    const result = data.filter(
      item =>
        item.title.toLowerCase().includes(searchVal.toLowerCase()) === true ||
        item.desc.toLowerCase().includes(searchVal.toLowerCase()) === true
    );
    return result instanceof Array ? result : [];
  }

  setSearchResults(searchVal) {
    const {
      pinned,
      notes,
      archive,
      fnAddItemToSearchResult,
      history,
      location
    } = this.props;
    if (searchVal.length > 2) {
      const notesResult = notes.data.length
        ? this.filterData(notes.data, searchVal)
        : [];

      const pinnedResult = pinned.data.length
        ? this.filterData(pinned.data, searchVal)
        : [];
      const archiveResults = archive.data.length
        ? this.filterData(archive.data, searchVal)
        : [];
      fnAddItemToSearchResult({
        key: searchVal,
        data: { pinnedResult, archiveResults, notesResult }
      });
      if (location.pathname !== '/search') {
        history.push('/search');
      }
    } else if (!searchVal.length) {
      history.goBack();
    }
  }

  handleSearchChange(e) {
    this.setState({ search: e.target.value });
    this.setSearchResults(e.target.value);
  }

  handleSearchBlur() {
    this.setState({ search: '' });
  }

  handleThemeChange = () => {
    const { isDark } = this.state;
    if (!isDark) {
      document.body.classList.add('dark-theme');
      document.querySelector('.sidebar').classList.add('dark-theme');
      this.setState({ isDark: !isDark });
    } else {
      document.body.classList.remove('dark-theme');
      document.querySelector('.sidebar').classList.remove('dark-theme');
      this.setState({ isDark: !isDark });
    }
  };

  render() {
    const { search } = this.state;
    const { handleMenuToggle } = this.props;
    const title =
      this.props.history.location.pathname === '/' ? 'Notes' : 'Archive';
    return (
      <header className="header">
        <div className="header__left">
          <button className="icon" onClick={() => handleMenuToggle()}>
            <MdMenu size={20} />
          </button>
          <h2 className="header__title heading-h2">{title}</h2>
        </div>
        <div className="header__input__container">
          <input
            type="search"
            name="search"
            value={search}
            className="search__input"
            placeholder="Search..."
            onChange={this.handleSearchChange}
            onBlur={this.handleSearchBlur}
          />
          <label className="switch">
            <input type="checkbox" />
            <span
              className="slider round"
              onClick={this.handleThemeChange}
            ></span>
          </label>
        </div>
      </header>
    );
  }
}

const mapStateToProps = state => ({
  notes: state.notes,
  archive: state.archive,
  pinned: state.pinned
});

const mapDispatchToProps = dispatch => ({
  fnAddItemToSearchResult: data => dispatch(searchRequest(data))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));
