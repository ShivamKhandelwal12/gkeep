This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

## UI COMPONENTS

### HEADER

global search for notes

Toggle button for `Dark` and `Light Theme`

## SIDEBAR

for navigation between notes and archive section

## MAIN CONTENT

displays the notes in card layout
